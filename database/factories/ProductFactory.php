<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name'=>$faker->userName,
        'price'=>$faker->numberBetween(1000, 75000),
        'description'=>$faker->text,
        'service'=>$faker->boolean(),

    ];
});
