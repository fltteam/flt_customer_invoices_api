<?php

use Faker\Generator as Faker;

$factory->define(App\Invoice::class, function (Faker $faker) {
    return [
        'date'=>$faker->dateTimeThisYear,
        'customer'=>factory(App\Customer::class)->create(),
        'product(s)'=>factory(App\Product::class)->create(),
    ];
});
