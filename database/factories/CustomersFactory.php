<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'first name'=>$faker->firstName,
        'last name'=>$faker->lastName,
        'email'=>$faker->safeEmail,
        'phone'=>$faker->phoneNumber,
        'address'=>$faker->address,
    ];
});
