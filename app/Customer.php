<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'first name',
        'last name',
        'email',
        'phone',
        'address',
    ];
}
