<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           //get (5) Customers per page
           return Customer::paginate(5);      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate request to create new Customer resource

        $this->validate($request,[
            'first name' => 'required|string',
            'last name' => 'required|string',
            'email' => 'required|email',           
        ]);

        // create a new Customer and return the same Customer
        return Customer::create($request->all()); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find and return the single Customer resource
        return Customer::findOrFail($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate request to update the  Customer resource

        $this->validate($request,[
            'first name' => 'required|string',
            'last name' => 'required|string',
            'email' => 'required|email',           
        ]);

        // find the Customer to update
        $customer =Customer::findOrFail($id);

         // update the Customer and return the updated Customer
        $customer->update($request->all()); 
        return  $customer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            // find the Customer resource to delete
            $customer =Customer::findOrFail($id);

            // Delete the Customer resource and return Message if Customer is deleted
             $customer->delete();
             return ['Message' => 'Customer deleted successfully'];
    }
}
