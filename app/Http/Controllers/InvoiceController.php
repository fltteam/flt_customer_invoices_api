<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Invoice;
use App\Product;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //get (5) invoices per page
         return Invoice::paginate(5);    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // Validate request to create new invoice resource

       $this->validate($request,[
        'date' => 'required|date',
        'customer' => 'required|numeric',
        'product(s)' => 'required|array',            
    ]);

    //verify that the customer exists in customers table
    $customer = Customer::find($request->customer);
    if(!$customer){
        return response( ['Error'=>'Customer with id '.$request->customer.' not found. Please input a valid Customer Id.'],404);
    }
    // Verify that the selected products are in the system
    foreach($request['product(s)'] as $product){

        $myproduct =Product::find($product);
        if(!$myproduct){
            return response( ['Error'=>'Product with id '.$product.' not found. Please input a valid Product Id.'],404);
        }
    }


    // create a new invoice and return the same invoice
    return Invoice::create($request->all()); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find and return the single Invoice resource
        return Invoice::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate request to update invoice resource

       $this->validate($request,[
        'date' => 'required|date',
        'customer' => 'required|numeric',
        'product(s)' => 'required|array',           
    ]);
            // find the Invoice to update
            $invoice =Invoice::findOrFail($id);

            //verify that the customer exists in customers table
            $customer = Customer::find($request->customer);
            if(!$customer){
                return response( ['Error'=>'Customer with id '.$request->customer.' not found. Please input a valid Customer Id.'],404);
            }
            // Verify that the Updated product ids are in the system
            foreach($request['product(s)'] as $product){

                $myproduct =Product::find($product);
                if(!$myproduct){
                    return response( ['Error'=>'Product with id '.$product.' not found. Please input a valid Product Id.'],404);
                }
            }

            // update the Invoice and return the updated Invoice
           $invoice->update($request->all()); 
           return  $invoice;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         // find the Invoice resource to delete
         $invoice =Invoice::findOrFail($id);

         // Delete the Invoice resource and return Message if Invoice is deleted
          $invoice->delete();
          return ['Message' => 'Invoice deleted successfully'];
 
    }
}
