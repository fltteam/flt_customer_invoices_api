<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get (5) products per page
        return Product::paginate(5);      

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate request to create new product resource

        $this->validate($request,[
            'name' => 'required|string',
            'price' => 'required|numeric',
            'service' => 'sometimes|boolean',
        ]);

        // create a new product and return the same product
        return Product::create($request->all()); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find and return the single product resource
        return Product::findOrFail($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate request to update the  product resource

        $this->validate($request,[
            'name' => 'required|string',
            'price' => 'required|numeric',
            'service' => 'sometimes|boolean',
        ]);

        // find the product to update
        $product =Product::findOrFail($id);

         // update the product and return the updated product
        $product->update($request->all()); 
        return  $product;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            // find the product resource to delete
        $product =Product::findOrFail($id);

        // Delete the product resource and return Message if product is deleted
         $product->delete();
         return ['Message' => 'Product deleted successfully'];
     }
}
