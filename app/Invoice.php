<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'date',
        'customer',
        'product(s)',
    ];
    protected $casts = [
        'product(s)' => 'array',
    ];
        //map the invoice to the users with this relationship
    public function custome_r(){
        return $this->belongsTo(Customer::class,'customer','id');
    }
}
