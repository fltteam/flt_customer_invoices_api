<?php

namespace Tests\Feature;

use App\Customer;
use App\Invoice;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;


class InvoicesTest extends TestCase
{   
     use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function users_can_access_invoices_list ()
    {
        $response = $this->get('/api/invoices');

        $response->assertStatus(200);
    }

     /** @test */
     public function an_invoice_can_be_added_through_a_form()
     {
        $this->withoutExceptionHandling();
        

         $response = $this->post('/api/invoices',$this->data());
 
         $this->assertCount(1,Invoice::all());
     }
      /** @test */
    public function an_invoice_can_be_updated_through_a_form()
    {
        $this->withoutExceptionHandling();

        $response =$this->post('/api/invoices',$this->data());
        $invoice = Invoice::all();
        $response = $this->patch('/api/invoices/'.$invoice[0]->id,array_merge($this->data(),['date'=>'2021-05-20']));
        $response->assertStatus(200);
    }

        /** @test */
        public function an_invoice_can_be_deleted_through_a_form()
        {
            $this->post('/api/invoices',$this->data());
            $invoice = Invoice::all();
            $response = $this->delete('/api/invoices/'.$invoice[0]->id);
            $response->assertStatus(200);
            $this->assertCount(0,Invoice::all());
        }
        
       /** @test */
       public function a_date_is_requierd()
       {
           $response = $this->post('/api/invoices',array_merge($this->data(),['date'=>'']));
   
           $response->assertSessionHasErrors('date');
           $this->assertCount(0,Invoice::all());
   
       }
       /** @test */
       public function a_customer_is_requierd()
       {
           $response = $this->post('/api/invoices',array_merge($this->data(),['customer'=>'']));
   
           $response->assertSessionHasErrors('customer');
           $this->assertCount(0,Invoice::all());
   
       }
       /** @test */
       public function a_product_is_requierd()
       {
           $response = $this->post('/api/invoices',array_merge($this->data(),['product(s)'=>'']));
   
           $response->assertSessionHasErrors('product(s)');
           $this->assertCount(0,Invoice::all());
   
       }

     private function data(){
            return [
                'date'=>'2021-04-15',
                'customer'=>$this->customer_data(),
                'product(s)'=>[$this->product_data()],
            ];
        
    }

    private function customer_data(){
        Customer::create([
            'first name'=>'Davido',
            'last name'=>'Cherope',
            'email'=>'davido@gmail.com',
            'phone'=>'256710140124',
            'address'=>'38769 Tania Port Tanyaborough, IL 56429-0375', 
        ]);

        $customer = Customer::all();


        return $customer[0]->id;
    }
    private function product_data(){
        Product::create([
            'name'=>'Mac Book Pro',
            'price'=>'12000',
            'description'=>'Latest Version of Apple Series',
            'service'=>'1'
        ]);
        $product= Product::all();
        return $product[0]->id ;
    }
}
