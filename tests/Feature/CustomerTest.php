<?php

namespace Tests\Feature;

use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function users_can_access_cutomers_list ()
    {
        $response = $this->get('/api/customers');

        $response->assertStatus(200);
    }

    /** @test */
    public function a_customer_can_be_added_through_a_form()
    {
        $response = $this->post('/api/customers',$this->data());

        $this->assertCount(1,Customer::all());
    }

    /** @test */
    public function a_customer_can_be_updated_through_a_form()
    {
        $this->withoutExceptionHandling();

        $response =$this->post('/api/customers',$this->data());
        $customer = Customer::all();
        $response = $this->patch('/api/customers/'.$customer[0]->id,array_merge($this->data(),['first name'=>'Isaac']));
        $response->assertStatus(200);
    }

        /** @test */
    public function a_customer_can_be_deleted_through_a_form()
    {
        $this->post('/api/customers',$this->data());
        $customer = Customer::all();
        $response = $this->delete('/api/customers/'.$customer[0]->id);
        $response->assertStatus(200);
        $this->assertCount(0,Customer::all());
    }

       /** @test */
    public function a_first_name_is_requierd()
    {
        $response = $this->post('/api/customers',array_merge($this->data(),['first name'=>'']));

        $response->assertSessionHasErrors('first name');
        $this->assertCount(0,Customer::all());

    }

       /** @test */
    public function a_last_name_is_requierd()
    {
        $response = $this->post('/api/customers',array_merge($this->data(),['last name'=>'']));

        $response->assertSessionHasErrors('last name');
        $this->assertCount(0,Customer::all());

    }
       /** @test */
    public function an_email_is_requierd()
    {
        $response = $this->post('/api/customers',array_merge($this->data(),['email'=>'']));

        $response->assertSessionHasErrors('email');
        $this->assertCount(0,Customer::all());

    }
       /** @test */
    public function a_phone_is_not_requierd()
    {
        $response = $this->post('/api/customers',array_merge($this->data(),['phone'=>'']));

        $response->assertSessionHasNoErrors('email');
        $this->assertCount(1,Customer::all());

    }
    
       /** @test */
    public function an_address_is_not_requierd()
    {
        $response = $this->post('/api/customers',array_merge($this->data(),['address'=>'']));

        $response->assertSessionHasNoErrors('address');
        $this->assertCount(1,Customer::all());

    }

    private function data(){
        return [
                    'first name'=>'Davido',
                    'last name'=>'Cherope',
                    'email'=>'davido@gmail.com',
                    'phone'=>'256710140124',
                    'address'=>'38769 Tania Port Tanyaborough, IL 56429-0375', 
                ];
    }
}
