<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function users_can_access_products_list ()
    {
        $response = $this->get('/api/products');

        $response->assertStatus(200);
    }

     /** @test */
     public function a_product_can_be_added_through_a_form()
     {
         $response = $this->post('/api/products',$this->data());
 
         $this->assertCount(1,Product::all());
     }
      /** @test */
    public function a_product_can_be_updated_through_a_form()
    {
        $this->withoutExceptionHandling();

        $response =$this->post('/api/products',$this->data());
        $product = Product::all();
        $response = $this->patch('/api/products/'.$product[0]->id,array_merge($this->data(),['name'=>'Iphone 12 Max Pro']));
        $response->assertStatus(200);
    }

     
        /** @test */
    public function a_product_can_be_deleted_through_a_form()
    {
        $this->post('/api/products',$this->data());
        $product = Product::all();
        $response = $this->delete('/api/products/'.$product[0]->id);
        $response->assertStatus(200);
        $this->assertCount(0,Product::all());
    }

     

       /** @test */
       public function a_name_is_requierd()
       {
           $response = $this->post('/api/products',array_merge($this->data(),['name'=>'']));
   
           $response->assertSessionHasErrors('name');
           $this->assertCount(0,Product::all());
   
       }
       /** @test */
       public function a_price_is_requierd()
       {
           $response = $this->post('/api/products',array_merge($this->data(),['price'=>'']));
   
           $response->assertSessionHasErrors('price');
           $this->assertCount(0,Product::all());
   
       }
       /** @test */
       public function a_service_is_boolean_requierd()
       {
           $response = $this->post('/api/products',array_merge($this->data(),['service'=>'']));
   
           $response->assertSessionHasErrors('service');
           $this->assertCount(0,Product::all());
   
       }

       private function data(){
        return [
                    'name'=>'Mac Book Pro',
                    'price'=>'12000',
                    'description'=>'Latest Version of Apple Series',
                    'service'=>'1'
                ];
    }
}
