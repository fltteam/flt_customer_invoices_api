**Introduction**

This API allows a user to perform CRUD operations on Customers, Products and Invoices


**Endpoint Descriptions**

| Endpoint | Description |
| ------ | ------ |
| GET/api/products | Returns a list of Products |
| GET/api/products{ID} | Return a single Product with an id of ID |
| POST/api/products | Creates a new Product |
| PATCH/api/products{ID} | Updates a Product with an id of ID|
| DELETE/api/products{ID} | Deletes a Product with an id of ID|
|                           |                               
| GET/api/customers | Returns a list of customers |
| GET/api/customers{ID} | Return a single customer with an id of ID |
| POST/api/customers | Creates a new customer |
| PATCH/api/customers{ID} | Updates a customer with an id of ID|
| DELETE/api/customers{ID} | Deletes a customer with an id of ID|
|                         |                                     |
| GET/api/invoices | Returns a list of invoices |
| GET/api/invoices{ID} | Return a single invoice with an id of ID |
| POST/api/invoices | Creates a new invoice |
| PATCH/api/invoices{ID} | Updates a invoice with an id of ID|
| DELETE/api/invoices{ID} | Deletes a invoice with an id of ID|


